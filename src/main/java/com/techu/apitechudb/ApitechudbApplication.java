package com.techu.apitechudb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApitechudbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApitechudbApplication.class, args);
	}

}

//Consulta a la BD

// Modelo
// Repositorio
// Consulta
// Conexion
// Servicio
// Controlador

