package com.techu.apitechudb.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "purchases")
public class PurchaseModel {

    @Id
    private String id;
    private String userId; //id usuario que compra
    private float amount;  //total compra
    private Map<String, Integer> purchaseItems; //<id, cantidad de productos comprados de ese id>

    public PurchaseModel(){}

    public PurchaseModel(String id, String userid, Map<String, Integer> purchaseItems) {
        this.id = id;
        this.userId = userid;
        this.purchaseItems = purchaseItems;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Map<String, Integer> getPurchaseItems() {
        return purchaseItems;
    }

    public void setPurchaseItems(Map<String, Integer> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }
}
