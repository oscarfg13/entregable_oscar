package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
//@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam (name = "$orderBy", required = false) String order) {
        System.out.println("getUsers");
        return new ResponseEntity<>(
                this.userService.findAll(order),
                HttpStatus.OK
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUser");
        System.out.println("La id del usuario a crear es " + user.getId());
        System.out.println("La descripción del usuario a crear es " + user.getName());
        System.out.println("La edad del usuario a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.addUser(user),
                HttpStatus.CREATED
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(
            @RequestBody UserModel userModel, @PathVariable String id
    ) {
        System.out.println("updateUser");
        System.out.println("La id del usuario a actualizar en parámetro URL es " + id);
        System.out.println("La id del usuario a actualizar es " + userModel.getId());
        System.out.println("El nombre del usuario a actualizar es " + userModel.getName());
        System.out.println("La edad del usuario a actualizar es " + userModel.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if (userToUpdate.isPresent()) {
            System.out.println("Usuario para actualizar encontrado, actualizando");
            this.userService.update(userModel);
        }

        return new ResponseEntity<>(
                userModel,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("La id del usuario a borrar es " + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no borrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
