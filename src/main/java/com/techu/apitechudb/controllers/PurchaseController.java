package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseServiceResponse> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase");
        System.out.println("La id del purchase a crear es " + purchase.getId());
        System.out.println("La id del usuario de la compra a crear es " + purchase.getUserId());
        System.out.println("Los pares <producto,cantidad> de la compra a crear son " + purchase.getPurchaseItems());

        PurchaseServiceResponse result = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(result, result.getResponseHttpStatusCode());
    }
    /*
    public ResponseEntity<PurchaseModel> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase");
        System.out.println("La id del purchase a crear es " + purchase.getId());
        System.out.println("La id del usuario de la compra a crear es " + purchase.getUserId());
        System.out.println("Los pares <producto,cantidad> de la compra a crear son " + purchase.getPurchaseItems());

        UserService us = new UserService();
        ProductService ps = new ProductService();

        boolean not_found = false;

        if ((us.findById(purchase.getUserId())).isPresent()) {
            if (purchaseService.findById(purchase.getId()).isPresent()) {
                for (Map.Entry<String, Integer> entry : purchase.getPurchaseItems().entrySet()) {
                    if (!ps.findById(entry.getKey()).isPresent()) {
                        not_found = true;
                    }
                }

            }
        } else not_found = true;

        if (not_found) {
            return new ResponseEntity<>(
                    "Requisitos no cumplidos",
                    HttpStatus.NOT_FOUND
            );
        } else {
            int amount = 0;
            for (Map.Entry<String, Integer> entry : purchase.getPurchaseItems().entrySet()) {
                Optional<ProductModel> pm = (ps.findById(entry.getKey()).get();
                amount += (pm.get().getPrice())*entry.getValue();
            }
            purchase.setAmount(amount);
            return new ResponseEntity<>(
                    this.purchaseService.addPurchase(purchase),
                    HttpStatus.CREATED
            );
        }
    }*/

    @GetMapping("/purchases")
    public  ResponseEntity<List<PurchaseModel>> getPurchases(){
        System.out.println("getPurchases");

        return new ResponseEntity<>(
                this.purchaseService.getPurchases(),
                HttpStatus.OK
        );
    }

}
