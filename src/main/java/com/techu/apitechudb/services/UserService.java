package com.techu.apitechudb.services;


import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(String order){
        System.out.println("findAll en UserService");

        List<UserModel> result;

        if (order != null) {
            System.out.println("Se ha pedido ordenar");
            result = this.userRepository.findAll(Sort.by("age"));
        }
        else {
            result = this.userRepository.findAll();

        }

        return result;
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }

    public UserModel addUser(UserModel product){
        System.out.println("add en UserService");

        return this.userRepository.save(product);
    }

    public UserModel update(UserModel userModel) {
        System.out.println("update en userModel");

        return this.userRepository.save(userModel);
    }

    public boolean delete(String id) {
        System.out.println("delete en UserService");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("Usuario encontrado, borrando");

            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}

/**
 BasicCalculator sut = new BasicCalculator();
 sut.suma(2, 2) -> 4 -> Invariante

 if (sut.suma(2,2) == 4) {
    GREEN -> TEST PASA
  } else {
        RED -> TEST NO PASA
 }

 CurrencyConverter sut = new CurrencyConverter();

 sut.convert("EUR", "USD", 3);

 convert a nivel interno

 CurrencyRateFetcher crf = MockGenerator.get("CurrencyRateFetcher");
 CurrencyRate usdRate = crf.fetch("USD"); -> esto es un mock
 //Al mock le digo el ratio de conversión que quiero -> 1.34

 BasicCalculator bc = MockGenerator.get("BasicCalculator");
 return bc.mult(3, usdRate);

Mocks -> Stub, Mock, Spy
 **/