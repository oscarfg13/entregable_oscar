package com.techu.apitechudb.services;

import com.techu.apitechudb.models.PurchaseModel;
import org.springframework.http.HttpStatus;

public class PurchaseServiceResponse {
    private String msg;
    private PurchaseModel purchase;
    private HttpStatus responseHttpStatusCode;

    public PurchaseServiceResponse(){}

    public PurchaseServiceResponse(String msg, PurchaseModel purchase, HttpStatus responseHttpStatusCode) {
        this.msg = msg;
        this.purchase = purchase;
        this.responseHttpStatusCode = responseHttpStatusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PurchaseModel getPurchase() {
        return purchase;
    }

    public void setPurchase(PurchaseModel purchase) {
        this.purchase = purchase;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }
}
