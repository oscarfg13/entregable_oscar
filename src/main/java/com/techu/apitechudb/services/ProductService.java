package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll(){
        System.out.println("findAll en ProductService");

        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("findById en ProductService");

        return this.productRepository.findById(id);
    }

    public ProductModel add(ProductModel product){
        System.out.println("add en ProductService");

        return this.productRepository.save(product);
    }


    public ProductModel update(ProductModel productModel) {
        System.out.println("update en ProductModel");

        return this.productRepository.save(productModel);
    }

    public boolean delete(String id) {
        System.out.println("delete en ProductService");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("Producto encontrado, borrando");

            this.productRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
